# Grocer Overview

Grocer is comprised of a "backoffice" web app where products and promotions can be created, as well as a Cart that can be filled with products by clicking a product's QR Code. If you also decide to build and install the mobile app (android), then the backoffice also serves as the place where you scan products in the mobile app.

The core functionality all lay within several key classes that are leveraged for both the web app AND the mobile apps. They are:

- Cart
- LineItem
- Grocer.Product
- Grocer.Promo | BasePromo | BX4MPromo | N4XPromo

The architecture of these classes is loosely modelled on the Saleforce Commerce Cloud platform, on which I have worked for the past 10 years. As such I understand that Writing a Promotions engine is not a trivial task, as such here are some liitations in the current implementation to consider:

- Limited types of Promotions
- No Promo Ranking system (where if multiple promos are applied to a single product, which one takes precedence)
- Lacks a robust exclusivity requirement, given the above

The Backoffice application fullfils the requirement of having a way to configure pricing, products and different types of promotions as outlined in the Use Cases. Also, building and installing mobile app can be quite involved, so the full requirements of the kata are fully represented by the web app alone, but instead of scanning the QR codes with an app, you'd simply click on them to emulate a real 'scan'.

This is my solution to the [Checkout Order Total Kata](https://github.com/PillarTechnology/kata-checkout-order-total).

## Quick Setup

### Web App

In the main directory, install and run the 'backoffice' web app

    $ npm install
    $ npm run ib
    $ npm run ibs
    $ npm run ui

Run Tests

    $ npm run testUI

The tests focus on the core functionality of the exercise (namely the cart/lineItem/promo) making sure promos are applied when conditions are met and removed when they are not. I have not written tests covering the creation of Products and Promotions, or the mini expressjs server that saves and loads storedata locally.

### Android App Setup

If you decide to try this out ,you'll need to run it on a real device, not an emulator. I did not want to spend time getting the camera orientation correct just to test on a emulator when I could test on a real device more quickly.

- Follow the instructions [here](https://ionicframework.com/docs/developing/android) to setup your local environment
- [You may need to setup your environment and device for installing/running apps from Android Studio](https://developer.android.com/studio/run/device)
- [You may also need to enable developer options on your android device](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/)

Then run the following commands

```
$ cd grocerScan
$ npm install ionic -g
$ npm install
$ ionic cap sync
$ ionic cap open android
```

Note that running `ionic cap open android` will eseentially open Android Studio where the application should automatically start to build. If your Android phone is connected and you've followed the instructions above, you should be able to select your device from the available devices to run the app on.