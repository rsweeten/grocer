var fs = require("fs");

exports.getCWD = async function (req, res, next) {

  try {
    var cwd = await process.cwd();
    return res
      .status(200)
      .json({ status: 200, data: cwd, message: "got the cwd" });
  } catch (e) {
    return res.status(400).json({ status: 400, message: e.message });
  }

};

exports.saveJson = async function (req, res, next) {

  var path1 = process.cwd() + "/grocerScan/src/assets/data/storedata.json";
  var json = JSON.stringify(req.body.data);

  try {
    var file = await fs.writeFileSync(path1, json);

    return res.status(200).json({ status: 200, data: file });
  } catch (e) {
    return res.status(400).json({ status: 400, message: e.message });
  }
};

exports.getJson = async function (req, res, next) {
  try {
    var file = await fs.readFileSync(process.cwd() + "/grocerScan/src/assets/data/storedata.json", 'utf-8');

    return res.status(200).json({ data: JSON.parse(file) });
  } catch (e) {
    return res.status(400).json({ error: e.message });
  }
}