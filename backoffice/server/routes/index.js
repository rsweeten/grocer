var express = require('express');
var router = express.Router();
var cors = require('cors');
var DiskController = require('../controllers/disk.controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/write-json', DiskController.saveJson );
router.get('/get-json', cors(), DiskController.getJson);
router.get("/cwd", DiskController.getCWD);


module.exports = router;
