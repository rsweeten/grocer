import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule} from '@angular/material/dialog';
import { Grocer } from '../../../classes/storedata';
import { UniqueValidator } from '../../../validators/uniquevalidator';
import { CommonRegex } from '../../../classes/regex';

import { DiskService } from '../../../services/disk.service';

import { CreateBasePromoComponent } from './create-base-promo.component';

export interface DialogData {
  products: Array<Grocer.Product>;
  existingPromoNames: Array<string>;
}

/*describe('CreateBasePromoComponent', () => {
  let component: CreateBasePromoComponent;
  let fixture: ComponentFixture<CreateBasePromoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatDialogModule,
        Grocer
      ],
      declarations: [ CreateBasePromoComponent ],
      providers: [
        DiskService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBasePromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});*/
