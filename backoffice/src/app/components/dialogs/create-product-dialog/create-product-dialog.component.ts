import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Grocer } from '../../../classes/storedata';
import { UniqueValidator } from '../../../validators/uniquevalidator';
import { CommonRegex } from '../../../classes/regex';

export interface ProductDialogData {
  product: Grocer.Product;
  existingNames: Array<string>;
}

@Component({
  selector: 'app-create-product-dialog',
  templateUrl: './create-product-dialog.component.html',
  styleUrls: ['./create-product-dialog.component.scss']
})
export class CreateProductDialogComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CreateProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductDialogData
  ) { }

  unitTypes = Grocer.PriceTypes();
  crx = new CommonRegex();

  productForm = this.fb.group({
    name: [''],
    price: ['', [
        Validators.required,
        Validators.pattern(this.crx.CurrencyRegex)
      ]
    ],
    unitType: ['']
  });

  // Getters
  get name() { return this.productForm.get('name')}
  get price() { return this.productForm.get('price')}

  ngOnInit(): void {
    console.log(this.data);

    this.productForm.setValidators([Validators.required, UniqueValidator(this.productForm.controls['name'], this.data.existingNames)]);
  }


  onSubmit(){
    if(this.productForm.invalid) {
      return;
    }
    let P : Grocer.Product = new Grocer.Product(
      Grocer.hash(this.productForm.value.name).toString(16),
      this.productForm.value.name,
      this.productForm.value.price,
      this.productForm.value.unitType );
    
    this.data.product = P;
    this.dialogRef.close(P);
    
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
