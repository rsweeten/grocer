import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Grocer } from '../../../classes/storedata';
import { UniqueValidator } from '../../../validators/uniquevalidator';
import { CommonRegex } from '../../../classes/regex';

export interface DialogData {
  products: Array<Grocer.Product>;
  existingPromoNames: Array<string>;
}

@Component({
  selector: 'app-create-n4x-promo',
  templateUrl: './create-n4x-promo.component.html',
  styleUrls: ['./create-n4x-promo.component.scss']
})
export class CreateN4xPromoComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CreateN4xPromoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  crx = new CommonRegex();

  promoForm = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    calloutMsg: [''],
    product: ['', Validators.required],
    n: [3],
    x: [''],
    start: [''],
    end: [''],
    perpetual: [true],
    limit: [0]
  });

  ngOnInit(): void {
    console.log(this.data);
    this.promoForm.setValidators([Validators.required, UniqueValidator(this.promoForm.controls['name'], this.data.existingPromoNames)]);
  }

  onSubmit(){
    if(this.promoForm.invalid) {
      return;
    }
    
    let Promo : Grocer.N4XPromo = new Grocer.N4XPromo(
      this.promoForm.value.name,
      this.promoForm.value.description,
      this.promoForm.value.calloutMsg,
      this.promoForm.value.product,
      'N4X',
      this.promoForm.value.start,
      this.promoForm.value.end,
      this.promoForm.value.perpetual,
      this.promoForm.value.limit,
      this.promoForm.value.n,
      this.promoForm.value.x );
    
    this.dialogRef.close(Promo);
    
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
