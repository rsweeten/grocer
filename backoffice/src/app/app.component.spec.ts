import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModules } from './material';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DiskService } from './services/disk.service';

import { Grocer } from './classes/storedata';
import { CurrencyPipe } from '@angular/common';
import { Cart } from './classes/cart/cart';
import { LineItem } from './classes/lineitem/line-item';

const json = {"_products":[{"id":"ae58ea90","name":"Broccoli","price":2.99,"unitType":"unit"},{"id":"a621d9be","name":"Brown Rice","price":7.49,"unitType":"unit"},{"id":"357664d5","name":"Rack of Lamb","price":23.37,"unitType":"weight"},{"id":"81520e1b","name":"Bagels","price":3.99,"unitType":"unit"},{"id":"ec1a4abc","name":"Haricots","price":4.99,"unitType":"unit"},{"id":"d4c6ff00","name":"Cannelini","price":1.99,"unitType":"unit"},{"id":"301f8b44","name":"Ground Beef","price":7.89,"unitType":"weight"},{"id":"905eb2f5","name":"Lacinato Kale","price":7.49,"unitType":"unit"},{"id":"d636d2da","name":"Croissants","price":5.5,"unitType":"unit"},{"id":"73cac50f","name":"Carrots","price":4.35,"unitType":"weight"},{"id":"930729f1","name":"Tortilla Chips","price":8.99,"unitType":"unit"},{"id":"eed51138","name":"Medium Spice Salsa","price":9.99,"unitType":"unit"}],"_promos":[{"_name":"Carrot Markdown","_product":{"id":"73cac50f","name":"Carrots","price":4.35,"unitType":"weight"},"_description":"$1 Off Carrots","_calloutMsg":"","_type":"base","_start":"","_end":"","_perpetual":true,"_limit":2,"_amountOff":1,"_isPercent":"","_n":0},{"_name":"SloppyJoe","_product":{"id":"301f8b44","name":"Ground Beef","price":7.89,"unitType":"weight"},"_description":"Buy 2lbs. Ground Beef, Get next lb. Half off","_calloutMsg":"Get next lb. Half off","_type":"BNU4X","_start":"","_end":"","_perpetual":true,"_limit":2,"_amountOff":50,"_isPercent":true,"_n":2},{"_name":"ChipNDip","_product":{"id":"930729f1","name":"Tortilla Chips","price":8.99,"unitType":"unit"},"_description":"Buy 2 Chips, Get Salsa Half Off","_calloutMsg":"Get Salsa Half Off","_type":"BX4M","_start":"","_end":"","_perpetual":true,"_limit":1,"_amountOff":50,"_isPercent":true,"_n":2,"_targetProduct":{"id":"eed51138","name":"Medium Spice Salsa","price":9.99,"unitType":"unit"}},{"_name":"BOGO","_product":{"id":"ae58ea90","name":"Broccoli","price":2.99,"unitType":"unit"},"_description":"Buy 1 Broccoli, Get 1 FREE","_calloutMsg":"","_type":"BX4M","_start":"","_end":"","_perpetual":true,"_limit":1,"_amountOff":100,"_isPercent":true,"_n":1,"_targetProduct":{"id":"ae58ea90","name":"Broccoli","price":2.99,"unitType":"unit"}},{"_name":"Brown Rice","_product":{"id":"a621d9be","name":"Brown Rice","price":7.49,"unitType":"unit"},"_description":"20% off Brown Rice","_calloutMsg":"","_type":"base","_start":"","_end":"","_perpetual":true,"_limit":1,"_amountOff":20,"_isPercent":true,"_n":0},{"_name":"BeanTown","_product":{"id":"d4c6ff00","name":"Cannelini","price":1.99,"unitType":"unit"},"_description":"Buy 4 Cannellini for $6","_calloutMsg":"","_type":"N4X","_start":"","_end":"","_perpetual":true,"_limit":0,"_amountOff":0,"_isPercent":false,"_n":3,"_x":6}]}

class MockDiskService {
  public getJSON() {
    return json;
  }
  public processData(data: Grocer.StoreData) {
    let a: Array<Grocer.N4XPromo|Grocer.BasePromo|Grocer.BX4MPromo> = [];
    let p: Array<Grocer.Product> =[];

    data._products.forEach(prod => {
      p.push(new Grocer.Product(prod.id, prod.name, prod.price, prod.unitType));
    });

    data._promos.forEach(promo => {
      switch(promo._type){
        case 'base':
          a.push(new Grocer.BasePromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff, promo._isPercent, 0));
          break;
        case 'BNU4X':
          a.push( new Grocer.BasePromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff,promo._isPercent, promo._n));
        break;
        case 'BX4M':
          a.push( new Grocer.BX4MPromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff, promo._isPercent, promo._targetProduct, promo._n));
          break;
        case 'N4X':
          a.push( new Grocer.N4XPromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._n, promo._x));
        break;
      }
    });
    data._promos = a;
    data._products = p;
    return data;
  }
}

describe('AppComponent', () => {
  let comp: AppComponent, diskService: MockDiskService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    comp = TestBed.inject(AppComponent);
    diskService = TestBed.inject(MockDiskService);
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'backoffice'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('backoffice');
  });

  it('should render title "grocer backoffice"', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('span.title').textContent).toContain('grocer backoffice');
  });

  it('should have storedata', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.storeData).toBeTruthy();
  });

  it('should have 12 products', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    expect(app.storeData._products.length).toEqual(12);
  });

  it('should have 6 promos', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    expect(app.storeData._promos.length).toEqual(6);
  });

});

describe('APP: Cart Test', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
    //app.addToCart(app.storeData._products[0]);
  });

  it('should be able to create an empty Cart', () => {
    expect(app.cart._lineItems.length).toEqual(0);
  });

  it('should be able to add a product to the Cart, resulting in product becoming a lineItem', () => {
    app.addToCart(app.storeData._products[0]);
    expect(app.cart._lineItems.length).toEqual(1);
    expect(app.cart._lineItems[0]).toBeInstanceOf(LineItem);
  });

  it('should be able to remove a product from the Cart', () => {
    app.addToCart(app.storeData._products[0]);
    expect(app.cart._lineItems.length).toEqual(1);
    app.removeFromCart(app.cart._lineItems[0]);
    expect(app.cart._lineItems.length).toEqual(0);
  });

  it('once a product is added to the cart, we should be able to use .increaseQty()', () => {
    app.addToCart(app.storeData._products[0]);
    app.increaseQty(app.cart._lineItems[0])
    expect(app.cart._lineItems[0].qty).toEqual(2);
  });

  it('should be able to use .decreaseQty() to decrement a lineItem Qty', () => {
    app.addToCart(app.storeData._products[0]);
    app.increaseQty(app.cart._lineItems[0])
    expect(app.cart._lineItems[0].qty).toEqual(2);
    app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].qty).toEqual(1);
  });

  it('if .decreaseQty() when lineItem Qty == 1, item shoulbd be removed from cart', () => {
    app.addToCart(app.storeData._products[0]);
    expect(app.cart._lineItems[0].qty).toEqual(1);
    app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems.length).toEqual(0);
  });

});

describe('APP: LineItem Test', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>,
      lineItem: LineItem,
      product: Grocer.Product;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
    app.addToCart(app.storeData._products[0]);
    lineItem = app.cart._lineItems[0];
    product = app.storeData._products[0];
  });

  it('a LineItem has a price property', () => {
    expect(lineItem.price).toBeTruthy();
  });

  it('a LineItem has a qty property', () => {
    expect(lineItem.qty).toBeTruthy();
  });

  it('a LineItem has a discount property that is initially 0', () => {
    expect(lineItem.discount).toEqual(0);
  });

  it('a LineItem has a name property that == product name', () => {
    expect(lineItem.name).toBeTruthy();
    expect(lineItem.name).toEqual(lineItem.product.name);
  });

  it(`a LineItem has a product property that's an instanceof Grocer.Product`, () => {
    expect(lineItem.product).toBeInstanceOf(Grocer.Product);
  });

  it(`a LineItem has a total property that == (qty*price)-discount`, () => {
    for(let i = 0; i < 3; i++){
      app.addToCart(product);
    }
    expect(lineItem.total).toEqual(8.97);
  });

  it('a LineItem .productID should == .product.id', () => {
    expect(lineItem.productID == lineItem.product.id).toBeTrue();
  });

  it('a LineItem qty should == qty in the Cart', () => {
    for(let i = 0; i < 5; i++){
      app.addToCart(app.storeData._products[0]);
    }
    expect(lineItem.qty).toEqual(6); // (b/c one was already added in the beforeEach)
  });

  it('should have its own unique ID when added to the cart', () => {
    for(let i = 0; i < 1; i++){
      app.addToCart(app.storeData._products[0]);
    }
    expect(lineItem.id).toEqual('0-' + lineItem.productID);
  });

});

describe('BX4M Promo Test 1 : Buy One Get One Free', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
  });

  it('when 1 Broccoli is added, the BOGO promo condition is not satisfied', () => {
    app.addToCart(app.storeData._products[0]);
    expect(app.cart._lineItems[0].applied).toEqual(0);
  });

  it('when 2 Broccoli are added, the lineItem discount should be 2.99', () => {
    for(let i = 0; i < 2; i++){
      app.addToCart(app.storeData._products[0]);
    }
    expect(app.cart._lineItems[0].discount).toEqual(2.99);
  });

  it('when 2 Broccoli are added, the BOGO order limit is reached', () => {
    for(let i = 0; i < 2; i++){
      app.addToCart(app.storeData._products[0]);
    }
    expect(app.cart._lineItems[0].applied == app.cart._lineItems[0]._appliedPromotion[0].limit).toBeTrue( );
  });

  it('when 2 Broccoli are added and then 1 is removed, the lineItem discount should be 0', () => {
    for(let i = 0; i < 2; i++){
      app.addToCart(app.storeData._products[0]);
    }
    app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].discount).toEqual(0);
  });

  it(`adding 4 Broccoli does not incur another BOGO b/c the promo's order limit was reached, and item price should be 8.97`, () => {

    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[0]);
    }
    expect(app.cart._lineItems[0].discount).toEqual(2.99);
    expect(app.cart._lineItems[0].total).toEqual(8.97);
    
  });

});

describe('BX4M Promo Test 2 : Buy N number of one product, get X amount off another product', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
  });

  it('when 2 Chips added and then Salsa is added, $5 off Salsa', () => {
    app.addToCart(app.storeData._products[10]);
    app.addToCart(app.storeData._products[10]);
    app.addToCart(app.storeData._products[11]);
    expect(app.cart._lineItems[1].discount).toEqual(5);
  });

  it('when 2 Chips added and Salsa was aready in the cart, $5 off Salsa', () => {
    app.addToCart(app.storeData._products[11]);
    app.addToCart(app.storeData._products[10]);
    app.addToCart(app.storeData._products[10]);
    expect(app.cart._lineItems[0].discount).toEqual(5);
  });

  it('$5 Salsa discount ONLY removed when Chip QTY no longer fulfills promo condition', () => {
    let salsa: LineItem,
        chips: LineItem;

    app.addToCart(app.storeData._products[11]);
    salsa = app.cart._lineItems[0];
    
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[10]);
    }
    chips = app.cart._lineItems[1];

    expect(salsa.discount).toEqual(5);
    
    app.decreaseQty(chips);
    expect(salsa.discount).toEqual(5);

    app.decreaseQty(chips);
    expect(salsa.discount).toEqual(5);

    app.decreaseQty(chips);
    expect(salsa.discount).toEqual(0);


  });

  it(`ChipNDip Order limit is 1, so if chips and salsa qty's exceed promo conditions, discount stays the same`, () => {
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[10]);
    }
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[11]);
    }
    expect(app.cart._lineItems[1].discount).toEqual(5);
    
  });

});

describe('Fixed Amt Markdown: $1 Off Carrots, order limit 2', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
  });

  it('$1 Off when 1 carrot bunch added', () => {
    app.addToCart(app.storeData._products[9]);
    expect(app.cart._lineItems[0].discount).toEqual(1);
  });

  it('$2 Off when 2 carrot bunches added', () => {
    app.addToCart(app.storeData._products[9]);
    app.addToCart(app.storeData._products[9]);
    expect(app.cart._lineItems[0].discount).toEqual(2);
  });

  it('Promo no longer valid when qty exceeds order limit (discount should still be $2 for qty > 2)', () => {
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[9]);
    }
    expect(app.cart._lineItems[0].discount).toEqual(2);
  });

  it('Discount reduced to $1 when qty decreased to 1', () => {
    let lineItem: LineItem;
    
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[9]);
    }
    
    lineItem = app.cart._lineItems[0];
    
    expect(lineItem.discount).toEqual(2);

    app.decreaseQty(lineItem);
    expect(lineItem.discount).toEqual(2);

    app.decreaseQty(lineItem);
    expect(lineItem.discount).toEqual(2);

    app.decreaseQty(lineItem);
    expect(lineItem.discount).toEqual(1);

  });

});

describe('% Markdown: 1/2 Off Brown Rice, order limit 1', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
  });

  it('1/2 Off when 1 bag Brown Rice added', () => {
    app.addToCart(app.storeData._products[1]);
    expect(app.cart._lineItems[0].discount).toEqual(1.5);
  });

  it('Promo no longer valid when qty exceeds order limit (discount should still be $1.50 for qty > 1)', () => {
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[1]);
    }
    expect(app.cart._lineItems[0].discount).toEqual(1.5);
  });

});

describe('N4X Test: 3 Cans Cannellini for $6, no order limit', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
  });

  it('4 cans for $6', () => {
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[5]);
    }
    expect(app.cart._lineItems[0].total).toEqual(6);
  });

  it('3 cans for $5.97)', () => {
    for(let i = 0; i < 3; i++){
      app.addToCart(app.storeData._products[5]);
    }
    expect(app.cart._lineItems[0].total).toEqual(5.97);
  });

  it('if 4 cans added, price is $6. If one is removed, price is $5.97', () => {
    for(let i = 0; i < 4; i++){
      app.addToCart(app.storeData._products[5]);
    }
    expect(app.cart._lineItems[0].total).toEqual(6);

    app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].total).toEqual(5.97);
  });

  it('Full of beans! Guest can buy 64 cans of beans for $96.. if one is removed, price is $95.97', () => {
    for(let i = 0; i < 64; i++){
      app.addToCart(app.storeData._products[5]);
    }
    expect(app.cart._lineItems[0].total).toEqual(96);

    app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].total).toEqual(95.97);
  });

});

describe('Sloppy Joe Test: Buy 2 lbs ground beef, get third pound for 1/2 off, order limit 2', () => {
  let diskService: MockDiskService,
      app : AppComponent,
      fixture: ComponentFixture<AppComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        MaterialModules
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AppComponent,
        MockDiskService,
        {provide: DiskService, use: MockDiskService}
      ]
    }).compileComponents();
    
    diskService = TestBed.inject(MockDiskService);
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // reset store data and cart each time
  beforeEach( async () => {
    app.storeData = diskService.getJSON();
    app.storeData = diskService.processData(app.storeData);
    app.cart = new Cart(app.storeData._promos);
  });

  it('when 3rd pound is added, discount applied, when 3rd pound removed again, discount removed', () => {
    for(let i = 0; i <2; i++){
      app.addToCart(app.storeData._products[6]);
    }
    expect(app.cart._lineItems[0].discount).toEqual(0);

    app.increaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].discount).toEqual(3.94);

    app.decreaseQty(app.cart._lineItems[0]);

    expect(app.cart._lineItems[0].discount).toEqual(0);
  });

  it('when 6th pound is added, additional discount applied. When removed again additional discount removed', () => {
    for(let i = 0; i < 5; i++){
      app.addToCart(app.storeData._products[6]);
    }
    expect(app.cart._lineItems[0].discount).toEqual(3.94);

    app.increaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].discount).toEqual(7.89);

    app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].discount).toEqual(3.94);
  });

  it('if 12 pounds added, discount will not exceed order limit. When qty decreased, discounts not removed until within range of promo conditions', () => {
    for(let i = 0; i < 12; i++){
      app.addToCart(app.storeData._products[6]);
    }
    expect(app.cart._lineItems[0].discount).toEqual(7.89);

    for(let i = 0; i < 6; i++){
      app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].discount).toEqual(7.89);
    }
    
    app.decreaseQty(app.cart._lineItems[0]);
    expect(app.cart._lineItems[0].discount).toEqual(3.94);
  });

});
