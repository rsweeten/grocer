import { Grocer } from '../storedata';
import { LineItem } from '../lineitem/line-item';

interface IDiscountData {
    lineItem: LineItem,
    promo: Grocer.BX4MPromo | Grocer.BasePromo | Grocer.N4XPromo;
    operation: string;
}
export class Cart extends EventTarget {
    
    _lineItems: Array<LineItem>;
    _crossPromo: Array<Grocer.BX4MPromo>;
    _currentPromotions: Array<Grocer.BX4MPromo | Grocer.BasePromo | Grocer.N4XPromo>;

    /**
     * 
     * @param lineItem 
     * @param promo 
     */
    private processDiscount(data: IDiscountData): void {
        let d: number = 0;
        
        if(data.promo.type == 'N4X' && 'x' in data.promo){
            d = ((data.promo.n + 1) * data.lineItem.price) - data.promo.x;
        }else if(data.promo._isPercent){
            d = ( data.lineItem.price / 100 ) * data.promo.amountOff;
        }else {
            d = data.promo.amountOff;
        }

        if(data.operation == 'add'){
            data.lineItem.increaseDiscount(d);
        }else{
            data.lineItem.decreaseDiscount(d);
        }

    }

    isCustomEvent = (event: Event) : event is CustomEvent => {
        return 'detail' in event;
    }

    /**
     * promoCheck 
     * 
     * The return boolean is used to determine whether we need to adjust line item pricing
     * 
     * @param lineItem  - the LineItem to check
     * @param adding - true = adding qty, false = reducing qty
     */
    private promoCheck(lineItem: LineItem) : void {

        let promo = this._currentPromotions.filter(promo => promo._product.id == lineItem.productID);

        
        if(promo.length) {
            // 1st check if this is a cross promo, b/c the conditions for qualified 
            // reference a different final qualifying product (the target)
            if ('targetProduct' in promo[0] && promo[0].targetProductID != lineItem.productID) {
                // we choose remainder of 1 b/c the discount will be applied to a target, which
                // may or may not be in the cart
                if(lineItem.qty === promo[0].n && promo[0].orderLimit(lineItem.applied) > 0) {
                    // attach the promo to lineItem whether target is in cart or not
                    // b/c promoCheck needs to test if order limit has been reached
                    lineItem.applyPromo(promo[0]);
                    
                    let target = this.checkForTarget(promo[0]);

                    if ( target instanceof LineItem && promo[0].orderLimit(target.applied) > 0) {
                        target.applyPromo(promo[0]);
                        this.dispatchEvent( new CustomEvent('process-discount', {detail: {lineItem: target, promo: promo[0], operation: 'add'}}));
                    } else {
                        this.pushXPromo(promo[0]);
                    }
                    
                }

            } else {

                if(promo[0].remainder(lineItem.qty) === 0 && promo[0].orderLimit(lineItem.applied) > 0){
    
                    lineItem.applyPromo(promo[0]);
    
                    this.dispatchEvent( new CustomEvent('process-discount', {detail: {lineItem: lineItem, promo: promo[0], operation: 'add'}}) );
                }

            }

        }

    }

    private reversePromo(lineItem: LineItem) {
        
        let lastAddedPromo = lineItem._appliedPromotion[lineItem._appliedPromotion.length-1];

        // reducing qty of originating product in a x-sell promo
        if ('targetProductID' in lastAddedPromo && lastAddedPromo.targetProductID != lineItem.productID && lastAddedPromo.productID == lineItem.productID) {
            let target = this.checkForTarget(lastAddedPromo);
            // we choose remainder of 1 b/c the discount will be applied to a target, which
            // may or may not be in the cart
            if(lastAddedPromo.remainder(lineItem.qty) === (lastAddedPromo.n-1)) {
                let popped = lineItem._appliedPromotion.pop();
                if ( target != undefined ) target._appliedPromotion.pop();
                this.dispatchEvent( new CustomEvent('process-discount', {detail: {lineItem: target, promo: popped, operation: 'sub'}}) );

            }

        } else if( 'targetProductID' in lastAddedPromo && lastAddedPromo.targetProductID == lineItem.productID && lastAddedPromo.productID != lineItem.productID ) {
            // reducing qty of a lineItem that's a target product in a x-sell promo
            if(lineItem.qty == 0) {
                for(let i = lineItem._appliedPromotion.length -1; i >= 0; i--) {
                    let p = lineItem._appliedPromotion.pop();
                    if ( p != undefined && 'targetProductID' in p ) {
                        this.crossPromos.push(p);
                    }
                }
            }
        } else {
            // reducing qty in a NON x-sell promo
            if(lastAddedPromo.n == 0 && lastAddedPromo.type == 'base') {
                if(lineItem.qty < lastAddedPromo.limit) {
                    let popped = lineItem._appliedPromotion.pop();
                    this.dispatchEvent( new CustomEvent('process-discount', {detail: {lineItem: lineItem, promo: popped, operation: 'sub'}}) );
                }
            }
            else if(lastAddedPromo.remainder(lineItem.qty) === lastAddedPromo.n && lineItem.applied && !lastAddedPromo.qtyExceedsPromoLimit(lineItem.qty) ){
                let popped = lineItem._appliedPromotion.pop();
                this.dispatchEvent( new CustomEvent('process-discount', {detail: {lineItem: lineItem, promo: popped, operation: 'sub'}}) );
            }
        }
        
    }

    private rollTarget() {

        for(let i = this.crossPromos.length - 1; i >= 0; i--) {

            let target = this.checkForTarget(this.crossPromos[i]);
            
            if ( target instanceof LineItem) {
                target.applyPromo(this.crossPromos[i]);
                this.dispatchEvent( new CustomEvent('process-discount', {detail: {lineItem: target, promo: this.crossPromos[i], operation: 'add'}}));
                this.crossPromos.splice(i,1);
            }
        }
        
    }

    constructor(promos: Array<Grocer.BX4MPromo | Grocer.BasePromo | Grocer.N4XPromo>) {
        
        super();

        this._lineItems = [];
        this._crossPromo = [];
        this._currentPromotions = promos;



        this.addEventListener('process-discount', (e: Event) => {
            if(!this.isCustomEvent(e)) return;
            this.processDiscount(e.detail);
        });
    }

    public get totalItems() { 
        let c = 0;
        this._lineItems.forEach(li => {
            c += li.qty;
        });
        return c;
    }

    public get subTotal() {
        let a = 0;
        this._lineItems.forEach(li => {
            a += li.price * li.qty;
        });
        return a - this.savings;
    }

    public get savings() {
        let a : number = 0;
        this._lineItems.forEach(li => {
            a += li.discount;
        });
        return a;
    }

    private get crossPromos() { return this._crossPromo; }
    private pushXPromo(promo: Grocer.BX4MPromo) { this._crossPromo.push(promo);}
    private checkForTarget(promo: Grocer.BX4MPromo) : LineItem | undefined {
        return this._lineItems.find(li => li.productID == promo.targetProductID);
    }

    /**
     * Adds a Product to the Cart as a LineItem. If there's a LineItem with the same productID
     * already in the Cart, it increases the QTY
     * @param product 
     */
    public addToCart(product: Grocer.Product): void {

        
        // check for lineItems with same product.id
        let match = this._lineItems.filter(li => li.productID == product.id),
            lineItem: LineItem;
        
        if(match.length) {
            lineItem = match[0];
        }else{
            lineItem = new LineItem(product);
            this._lineItems.push(lineItem);
            lineItem.id = this._lineItems.length - 1 + '-' + product.id;
        }
        
        this.addQty(lineItem);

    }

    /**
     * Adds QTY to a line item, and then checkc for promos
     * @param lineItem 
     */
    public addQty(lineItem: LineItem) {
        lineItem.increaseQty();

        // Roll Targets
        if(this.crossPromos.length){
            this.rollTarget();
        }
        // check promos
        this.promoCheck(lineItem);
    }

    /**
     * Decreases tQTY of a LineItem. If the QTY is 0, calls removeLineItem
     * @param lineItem 
     */
    public removeQty(lineItem: LineItem) {
        lineItem.decreaseQty();

        //re-evaluate promos
        if(lineItem.applied > 0){
            this.reversePromo(lineItem);
        }

        if(lineItem.qty === 0) {
            this.removeLineItem(lineItem);
            return;
        }
    }

    public removeLineItem(lineItem: LineItem): void {
 
        this._lineItems.splice(this._lineItems.indexOf(lineItem), 1);

    }


}
