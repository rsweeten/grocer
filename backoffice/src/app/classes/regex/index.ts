export class CommonRegex {
    public readonly pwRegex = /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%&? "]).*$/;
    public readonly NumberRegex = /^-?(0|[1-9]\d*)?$/;
    public readonly CurrencyRegex = /^\$?(([1-9]\d{0,2}(,\d{3})*)|0)?\.\d{1,2}$/;

    constructor() { }
}