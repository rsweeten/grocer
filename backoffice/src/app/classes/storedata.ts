import { LineItem } from './lineitem/line-item';
export module Grocer {

    export function hash(text: string) {

        var hash = 5381,
            index = text.length;
    
        while (index) {
            hash = (hash * 33) ^ text.charCodeAt(--index);
        }
    
        return hash >>> 0;
    }
    export class StoreData {
        _products: Array<Product>;
        _promos: Array<any>;

        constructor(products: any, promos: any){
            this._products = products;
            this._promos = promos;
        }

    }

    export enum UnitPriceType {
        weight,
        unit
    }

    export function PriceTypes() {
        const map: { id: number, name: string }[] = [];
    
        for (const n in Grocer.UnitPriceType) {
            if (typeof Grocer.UnitPriceType[n] === 'number') {
                map.push({ id: <any>Grocer.UnitPriceType[n], name: n });
            }
        }
        return map;
    }

    export class Product {
        id: string;
        name: string;
        price: number;
        unitType: string

        constructor(id: string, name: string, price: number, type: string) {
            this.id = id;
            this.name = name;
            this.price = price;
            this.unitType = type;
        }
    }
    export class BasePromo {
        _name: string;
        _description: string;
        _calloutMsg: string;
        _type: string;
        _product: Product;
        _start: Date;
        _end: Date;
        _perpetual: boolean;
        _limit: number;
        _amountOff: number;
        _isPercent: boolean;
        _n: number;

        constructor(
            name: string,
            description: string,
            calloutMsg: string,
            product: Product,
            type: string, 
            start: Date, 
            end: Date, 
            perpetual: boolean, 
            limit: number, 
            amount: number,
            percent: boolean,
            n: number) {
            this._name = name;
            this._product = product;
            this._description = description;
            this._calloutMsg = calloutMsg;
            this._type = type;
            this._start = start;
            this._end = end;
            this._perpetual = perpetual;
            this._limit = limit;
            this._amountOff = amount;
            this._isPercent = percent;
            this._n = n;
        }

        public get name(){ return this._name; }
        public get type() { return this._type; }
        public get amountOff() { return this._amountOff; }
        public get start() { return this._start;}
        public get end() { return this._end; }
        public get product() { return this._product; }
        public get productID() { return this._product.id; }
        public get n() { return this._n; }
        public get limit() {return this._limit; }

        /**
         * 
         * @param qty 
         */
        public remainder(qty: number) { return qty % (this.n + 1); }
        
        /**
         * returns 0 if the order limit has been reached
         * if promo.limit was set to 0, it means no limit, so it will always return non-0
         * @param applied - the # of promos applied to a line item 
         */
        public orderLimit(applied: number) {
            // limit 0 = no limit, so always return non-zero
            if(this.limit == 0) {
                return 1;
            }else{
                return this.limit - applied;
            }
        }

        public qtyExceedsPromoLimit(qty: number) {
            if(this.limit == 0) {
                return false;
            }else {
                return qty > ((this.n + 1) * this.limit);
            }
        }
        
        public set name(name: string) { this._name = name; }
        public set type(type: string) { this._type = type; }
        public set amountOff(amt: number) { this._amountOff = amt; }
        public set start(start: Date){ this._start = start; }
        public set end (end: Date) { this._end = end; }
        public set product(product: Product) { this._product = product; }

    }
    export class BX4MPromo extends BasePromo {
        _targetProduct: Product;

        constructor(name: string, desc: string, callout: string, product: Product, type: string, start: Date, end: Date, perpetual: boolean, limit: number, amount: number, percent: boolean, target: Product, n: number) {
            super(name, desc, callout, product, 'BX4M', start, end, perpetual, limit, amount, percent, n);
            this._targetProduct = target;
        }

        public get targetProduct() { return this._targetProduct; }
        public get targetProductID() { return this._targetProduct.id; }

    }

    export class N4XPromo extends BasePromo {
        _x: number;

        constructor(name: string, desc: string, callout: string, product: Product, type: string, start: Date, end: Date, perpetual: boolean, limit: number, n: number, x: number) {
            super(name, desc, callout, product, 'N4X', start, end, perpetual, limit, 0, false, n);
            this._x = x;
        }

        public get x() { return this._x; }
        
    }

}
