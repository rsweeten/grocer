import { stringify } from '@angular/compiler/src/util';
import { Grocer } from '../storedata';

class Target {

}

export class LineItem {
    _id: string;
    private _product: Grocer.Product;
    _appliedPromotion: Array<Grocer.BasePromo | Grocer.BX4MPromo | Grocer.N4XPromo>;
    _price: number;
    _discount: number;
    _qty: number;
    
    constructor(product: Grocer.Product) {
        this._id = '';
        this._product = product;
        this._appliedPromotion = [];
        this._price = product.price;
        this._discount = 0;
        this._qty = 0;
    }

    public get id() { return this._id; }
    public get qty() { return this._qty; }
    public get product() { return this._product; }
    public get productID() { return this._product.id; }
    public get name() { return this._product.name; }
    public get price() { return this._price; }
    public get total() { return (this.price * this.qty) - this.discount; }
    public get basePrice() { return this._product.price; }
    public get applied() { return this._appliedPromotion.length; }
    public get discount() { return parseFloat(this._discount.toFixed(2)); }

    
    public set id(id: string) { this._id = id; }
    public set price(price: number) { this._price = price; }
    
    public increaseQty() { this._qty += 1; }
    public decreaseQty() { this._qty -= 1; }
    
    public applyPromo(promo: Grocer.BasePromo | Grocer.BX4MPromo | Grocer.N4XPromo) {
        this._appliedPromotion.push(promo);
    }

    public increaseDiscount(amt: number) {
        this._discount += amt;
    }

    public decreaseDiscount(amt: number) {
        this._discount -= amt;
    }
}
