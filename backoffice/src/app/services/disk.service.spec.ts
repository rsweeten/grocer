import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { DiskService } from './disk.service';
import { Grocer } from '../classes/storedata';

const json = {"_products":[{"id":"ae58ea90","name":"Broccoli","price":2.99,"unitType":"unit"},{"id":"a621d9be","name":"Brown Rice","price":7.49,"unitType":"unit"},{"id":"357664d5","name":"Rack of Lamb","price":23.37,"unitType":"weight"},{"id":"81520e1b","name":"Bagels","price":3.99,"unitType":"unit"},{"id":"ec1a4abc","name":"Haricots","price":4.99,"unitType":"unit"},{"id":"d4c6ff00","name":"Cannelini","price":1.99,"unitType":"unit"},{"id":"301f8b44","name":"Ground Beef","price":7.89,"unitType":"weight"},{"id":"905eb2f5","name":"Lacinato Kale","price":7.49,"unitType":"unit"},{"id":"d636d2da","name":"Croissants","price":5.5,"unitType":"unit"},{"id":"73cac50f","name":"Carrots","price":4.35,"unitType":"weight"},{"id":"930729f1","name":"Tortilla Chips","price":8.99,"unitType":"unit"},{"id":"eed51138","name":"Medium Spice Salsa","price":9.99,"unitType":"unit"}],"_promos":[{"_name":"Carrot Markdown","_product":{"id":"73cac50f","name":"Carrots","price":4.35,"unitType":"weight"},"_description":"$1 Off Carrots","_calloutMsg":"","_type":"base","_start":"","_end":"","_perpetual":true,"_limit":2,"_amountOff":1,"_isPercent":"","_n":0},{"_name":"SloppyJoe","_product":{"id":"301f8b44","name":"Ground Beef","price":7.89,"unitType":"weight"},"_description":"Buy 2lbs. Ground Beef, Get next lb. Half off","_calloutMsg":"Get next lb. Half off","_type":"BNU4X","_start":"","_end":"","_perpetual":true,"_limit":2,"_amountOff":50,"_isPercent":true,"_n":2},{"_name":"ChipNDip","_product":{"id":"930729f1","name":"Tortilla Chips","price":8.99,"unitType":"unit"},"_description":"Buy 2 Chips, Get Salsa Half Off","_calloutMsg":"Get Salsa Half Off","_type":"BX4M","_start":"","_end":"","_perpetual":true,"_limit":1,"_amountOff":50,"_isPercent":true,"_n":2,"_targetProduct":{"id":"eed51138","name":"Medium Spice Salsa","price":9.99,"unitType":"unit"}},{"_name":"BOGO","_product":{"id":"ae58ea90","name":"Broccoli","price":2.99,"unitType":"unit"},"_description":"Buy 1 Broccoli, Get 1 FREE","_calloutMsg":"","_type":"BX4M","_start":"","_end":"","_perpetual":true,"_limit":1,"_amountOff":100,"_isPercent":true,"_n":1,"_targetProduct":{"id":"ae58ea90","name":"Broccoli","price":2.99,"unitType":"unit"}},{"_name":"Brown Rice","_product":{"id":"a621d9be","name":"Brown Rice","price":7.49,"unitType":"unit"},"_description":"20% off Brown Rice","_calloutMsg":"","_type":"base","_start":"","_end":"","_perpetual":true,"_limit":1,"_amountOff":20,"_isPercent":true,"_n":0},{"_name":"BeanTown","_product":{"id":"d4c6ff00","name":"Cannelini","price":1.99,"unitType":"unit"},"_description":"Buy 4 Cannellini for $6","_calloutMsg":"","_type":"N4X","_start":"","_end":"","_perpetual":true,"_limit":0,"_amountOff":0,"_isPercent":false,"_n":3,"_x":6}]};
let storedata = new Grocer.StoreData(json._products, json._promos);

describe('DiskService', () => {
  let service: DiskService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    });
    service = TestBed.inject(DiskService);
    let data = service.processData(storedata);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
