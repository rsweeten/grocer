import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Grocer } from '../classes/storedata';

@Injectable({
  providedIn: 'root'
})
export class DiskService {

  api_url = 'http://localhost:3000';
  cwd = `${this.api_url}/cwd`;
  write = `${this.api_url}/api/write-json`;
  getJson = `${this.api_url}/get-json`;
  headers: HttpHeaders = new HttpHeaders();

  constructor(
    private http: HttpClient
  ) { }

  getCWD(): Observable<any> {
    const headers = this.headers;
    return this.http.get(`${this.cwd}`)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  writeToJSONFile(data: Grocer.StoreData): Observable<any> {

    const headers = this.headers;

    return this.http.post(`${this.write}`, {data: data}, {headers})
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  processData(data: Grocer.StoreData) : Grocer.StoreData {

    let a: Array<Grocer.N4XPromo|Grocer.BasePromo|Grocer.BX4MPromo> = [];
    let p: Array<Grocer.Product> =[];

    data._products.forEach(prod => {
      p.push(new Grocer.Product(prod.id, prod.name, prod.price, prod.unitType));
    });

    data._promos.forEach(promo => {
      switch(promo._type){
        case 'base':
          a.push(new Grocer.BasePromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff, promo._isPercent, 0));
          break;
        case 'BNU4X':
          a.push( new Grocer.BasePromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff,promo._isPercent, promo._n));
        break;
        case 'BX4M':
          a.push( new Grocer.BX4MPromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff, promo._isPercent, promo._targetProduct, promo._n));
          break;
        case 'N4X':
          a.push( new Grocer.N4XPromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._n, promo._x));
        break;
      }
    });
    data._promos = a;
    data._products = p;
    return data;
  }

  getJSON(): Observable<any> {
    return this.http.get(`${this.getJson}`)
      .pipe(
        map(res => {
          return res;
        })
      );
  }
}
