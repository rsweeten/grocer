import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { QRCodeModule } from 'angular2-qrcode';
import { DiskService } from './services/disk.service';

//Dialogs
import { CreateProductDialogComponent } from './components/dialogs/create-product-dialog/create-product-dialog.component';
import { CreateBasePromoComponent } from './components/dialogs/create-base-promo/create-base-promo.component';
import { CreateBx4mPromoComponent } from './components/dialogs/create-bx4m-promo/create-bx4m-promo.component';
import { CreateN4xPromoComponent } from './components/dialogs/create-n4x-promo/create-n4x-promo.component';

import { MaterialModules } from './material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CreateProductDialogComponent,
    CreateBasePromoComponent,
    CreateBx4mPromoComponent,
    CreateN4xPromoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    QRCodeModule,
    MaterialModules,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  entryComponents: [
    CreateProductDialogComponent,
    CreateBasePromoComponent,
    CreateBx4mPromoComponent,
    CreateN4xPromoComponent
  ],
  providers: [
    DiskService,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
