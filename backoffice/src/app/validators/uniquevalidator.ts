import { AbstractControl, ValidatorFn } from '@angular/forms';

export function UniqueValidator(control: AbstractControl, existingNames: Array<string>): ValidatorFn {
    return (): { [key: string]: any } | null => {
        
        let unique = existingNames.filter(element => element == control.value);
        //console.log(unique);

        if (unique.length) {
            control.setErrors({ uniqueValidator: true, nameFound: unique[0] });
        } else {
            control.setErrors(null);
        }
        return unique.length ? { uniqueValidator: true, nameFound: unique[0] } : null;

    }
}