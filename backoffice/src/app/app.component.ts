import { OnInit, Component } from '@angular/core';

import { Grocer } from './classes/storedata';
import { Cart } from './classes/cart/cart';
import { LineItem } from './classes/lineitem/line-item';

import { MatDialog } from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import { DiskService } from './services/disk.service';

import { CreateProductDialogComponent } from './components/dialogs/create-product-dialog/create-product-dialog.component';
import { CreateBasePromoComponent } from './components/dialogs/create-base-promo/create-base-promo.component';
import { CreateBx4mPromoComponent } from './components/dialogs/create-bx4m-promo/create-bx4m-promo.component';
import { CreateN4xPromoComponent } from './components/dialogs/create-n4x-promo/create-n4x-promo.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  comp: any;
  [x: string]: any;
  title: string = 'backoffice';
  storeData: Grocer.StoreData = new Grocer.StoreData([],[]);
  cart: Cart = new Cart(this.storeData._promos);

  cartColumns: string[] = ['Item', 'basePrice', 'Qty', 'discount', 'Price'];
  cartList: MatTableDataSource<LineItem> = new MatTableDataSource();

  constructor(
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private diskService: DiskService
  ){}

  ngOnInit(): void {
    // get storedata
    this.diskService.getJSON().subscribe(res => {
      this.storeData = res.data;
      this.storeData = this.diskService.processData(this.storeData);
      this.cart._currentPromotions = this.storeData._promos;
      this.cartList.data = this.cart._lineItems;
      //console.log(this.storeData);
    });

    this.diskService.getCWD().subscribe(res => {
      //console.log(res);
    })
  }

  addToCart(product: Grocer.Product) {
    this.cart.addToCart(product);
    this.cartList.data = this.cart._lineItems;
    //console.log(this.cart);
  }

  removeFromCart(lineItem: LineItem) {
    this.cart.removeLineItem(lineItem);
    this.cartList.data = this.cart._lineItems;
  }

  increaseQty(lineItem: LineItem) {
    this.cart.addQty(lineItem);
    this.cartList.data = this.cart._lineItems;
  }

  decreaseQty(lineItem: LineItem) {
    this.cart.removeQty(lineItem);
    this.cartList.data = this.cart._lineItems;
  }

  existingProductNames(): Array<string> {
    let a: Array<string> = [];
    this.storeData._products.forEach(product => {
      a.push(product.name);
    });
    return a;
  }

  existingPromoNames(): Array<string> {
    let a: Array<string> = [];
    this.storeData._promos.forEach(promo => {
      a.push(promo.name);
    });
    return a;
  }

  createProduct(): void {
      const dialogRef = this.dialog.open(CreateProductDialogComponent, {
        width: '400px',
        data: {product: this.newProduct, existingNames: this.existingProductNames()}
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          this.storeData._products.push(result);
          //this.newProduct = new Grocer.Product('', 1.99, 'unit');
        }
      });
  }

  deleteProduct(product: Grocer.Product) {
    this.storeData._products.splice( this.storeData._products.indexOf(product), 1);
  }

  createBasePromo() {
    const dialogRef = this.dialog.open(CreateBasePromoComponent, {
      width: '500px',
      data: {products: this.storeData._products, existingPromoNames: this.existingPromoNames()}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.storeData._promos.push(result);
      }
    });
  }

  createBX4MPromo() {
    const dialogRef = this.dialog.open(CreateBx4mPromoComponent, {
      width: '500px',
      data: {products: this.storeData._products, existingPromoNames: this.existingPromoNames()}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.storeData._promos.push(result);
      }
    });
  }

  createN4XPromo() {
    const dialogRef = this.dialog.open(CreateN4xPromoComponent, {
      width: '500px',
      data: {products: this.storeData._products, existingPromoNames: this.existingPromoNames()}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.storeData._promos.push(result);
      }
    });
  }

  deletePromo(promo: Grocer.BasePromo | Grocer.BX4MPromo | Grocer.N4XPromo) {
    this.storeData._promos.splice( this.storeData._promos.indexOf(promo), 1);
  }

  saveStoreData(): void {
    this.diskService.writeToJSONFile(this.storeData).subscribe(res => {
      if('status' in res && res.status == 200){
        // show a
        this.snackBar.open('Store Data has been saved!', '', {
          duration: 2000
        });
      }
    });
  }
}
