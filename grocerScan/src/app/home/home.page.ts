import { Component, OnInit } from '@angular/core';

import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';

import { Grocer } from '../../../../backoffice/src/app/classes/storedata';
import { Cart } from '../../../../backoffice/src/app/classes/cart/cart';
import { LineItem } from '../../../../backoffice/src/app/classes/lineitem/line-item';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  storeData: Grocer.StoreData = new Grocer.StoreData([],[]);
  cart: Cart = new Cart(this.storeData._promos);

  cartColumns: string[] = ['Item', 'basePrice', 'Qty', 'discount', 'Price'];
  cartList: MatTableDataSource<LineItem> = new MatTableDataSource();

  scanSub;
  qrText: string;
  scanning: boolean;

  constructor(
    public platform: Platform,
    private qrScanner: QRScanner,
    private http: HttpClient
  ) {
    
  }

  ngOnInit(): void {
    
    this.http.get('assets/data/storedata.json').subscribe( data => {
      this.processData(data);
      console.log(this.storeData);
      this.cart._currentPromotions = this.storeData._promos;
      console.log(this.cart);
      this.addToCart(this.storeData._products[0]);
      this.addToCart(this.storeData._products[0]);
      this.cartList.data = this.cart._lineItems;
    });
  }

  startScanning() {
    // Optionally request the permission early
    this.qrScanner.prepare().
      then((status: QRScannerStatus) => {
        if (status.authorized) {
          this.qrScanner.show();
          this.scanning = true;

          debugger
          this.scanSub = this.qrScanner.scan()
            .subscribe((textFound: string) => {

              this.qrText = textFound;
              this.addScanToCart(textFound);

              document.getElementById('stopit').click();
              
            }, (err) => {
              alert(JSON.stringify(err));
            });

        } else if (status.denied) {
        } else {

        }
      })
      .catch((e: any) => console.log('Error is', e));
  }

  stopScanning() {
    this.scanSub.unsubscribe();
    this.qrScanner.hide();
    this.qrScanner.destroy();
    this.scanning = false;
    //document.getElementsByTagName('body')[0].style.opacity = '1';
  }

  addScanToCart(code: string) {
    let p : Grocer.Product = this.storeData._products.find(p => p.id == code);

    if(p != undefined){
      this.addToCart(p);
    }else{

    }

  }

  addToCart(product: Grocer.Product) {
    this.cart.addToCart(product);
    this.cartList.data = this.cart._lineItems;
    //console.log(this.cart);
  }

  removeFromCart(lineItem: LineItem) {
    this.cart.removeLineItem(lineItem);
    this.cartList.data = this.cart._lineItems;
  }

  increaseQty(lineItem: LineItem) {
    this.cart.addQty(lineItem);
    this.cartList.data = this.cart._lineItems;
  }

  decreaseQty(lineItem: LineItem) {
    this.cart.removeQty(lineItem);
    this.cartList.data = this.cart._lineItems;
  }

  processData(data) : void {

    let a: Array<Grocer.N4XPromo|Grocer.BasePromo|Grocer.BX4MPromo> = [];
    let p: Array<Grocer.Product> =[];

    data._products.forEach(prod => {
      p.push(new Grocer.Product(prod.id, prod.name, prod.price, prod.unitType));
    });

    data._promos.forEach(promo => {
      switch(promo._type){
        case 'base':
          a.push(new Grocer.BasePromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff, promo._isPercent, 0));
          break;
        case 'BNU4X':
          a.push( new Grocer.BasePromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff,promo._isPercent, promo._n));
        break;
        case 'BX4M':
          a.push( new Grocer.BX4MPromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._amountOff, promo._isPercent, promo._targetProduct, promo._n));
          break;
        case 'N4X':
          a.push( new Grocer.N4XPromo(promo._name, promo._description, promo._calloutMsg, promo._product, promo._type, promo._start, promo._end, promo._perpetual, promo._limit, promo._n, promo._x));
        break;
      }
    });
    this.storeData._promos = a;
    this.storeData._products = p;
  }

}
